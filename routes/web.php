<?php



Route::get('/', function () {
    return view('site.home');
})->name('home');

Route::get('html', function () {
    return view('site.html');
})->name('html');

Route::get('javascript', function () {
    return view('site.javascript');
})->name('javascript');

Route::get('dica-css', function () {
    return view('site.css');
})->name('dica-css');
Route::get('videoaulas', function () {
    return view('site.videoaulas');
})->name('videoaulas');
Route::get('contato', function () {
    return view('site.contato');
})->name('contato');
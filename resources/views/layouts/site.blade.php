<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/site.css">

    <title>site @yield('titulo')</title>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="cabecalho col-12">

                <h1><img src="img/logo.png" alt="Web Mag" width="100"></h1>


                <nav>
                    <ul>
                        <li><a href="{{ route('home') }}">HOME</a></li>
                        <li><a href="{{ route('html') }}">HTML</a></li>
                        <li><a href="{{ route('javascript') }}">JAVASCRIPT</a></li>
                        <li><a href="{{ route('dica-css') }}">CSS</a></li>
                        <li><a href="{{ route('videoaulas') }}">VIDEOAULAS</a></li>
                        <li><a href="{{ route('contato') }}">CONTATO</a></li>



                    </ul>


                </nav>


            </div>
        </div>
    </div>



    <div class="container">

        @yield('container')
    </div>



    

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <hr>
                        <p>TODOS OS DIREITOS RESERVADOS</p>

                </div>
            </div>
        </div>
       

    </footer>
    










    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>

</body>

</html>

@extends('layouts.site')
@section('titulo','videoaulas')
@section('container')

<h2 class="alinhar">VIDEO AULAS</h2>
<div class="container">
    <div class="row">

        <div class="item col-4">
            <iframe width="350" height="200" src="https://www.youtube.com/embed/iZ1ucWosOww" frameborder="0"
                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen></iframe>
        </div>

        <div class="col-8">

            <p>Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem
                sendo
                utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os
                embaralhou
                para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos</p>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quia, repellendus architecto? Molestias,
                molestiae
                velit
                possimus ipsam officiis dolore non vitae error quos, ex odit ullam neque earum modi ipsa eveniet.
            </p>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="item-2 col-4">
            <iframe width="350" height="200" src="https://www.youtube.com/embed/ze9--J4PJ_4" frameborder="0"
                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen></iframe>
            <p>GitHub, GitLab ou Bitbucket? Qual nós usamos! // CAC #6</p>

        </div>

        <div class="item-2 col-4">
                <iframe width="350" height="200" src="https://www.youtube.com/embed/jyTNhT67ZyY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <p>MVC // Dicionário do Programador</p>
    
            </div>
            <div class="item-2 col-4">
                    <iframe width="350" height="200" src="https://www.youtube.com/embed/5K7OGSsWlzU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    <p>O que um Analista de Sistemas faz?// Vlog #77</p>
        
                </div>

    </div>
</div>
@endsection

@extends('layouts.site')
@section('titulo','home')
@section('container')



<div class="row">
    <div class="item col-4">
        <img class="img-fluid" src="img/post-3.jpg" alt="dicas HTML">
        <h2>Dicas de HTML</h2>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quia, repellendus architecto?
            Molestias,
            molestiae velit possimus ipsam officiis dolore non vitae error quos, ex odit ullam neque
            earum
            modi
            ipsa
            eveniet.
        </p>

    </div>


    <div class="item-1 col-4">
        <img class="img-fluid" src="img/post-4.jpg" alt="MVC">
        <h2>O que é MVC</h2>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam nobis ratione eius itaque, id
            facilis
            eligendi libero corrupti, eaque cumque amet repellat? Optio eius voluptatibus possimus
            repellendus
            iusto
            nam sequi.</p>

    </div>

    <div class="item-2 col-4">
        <img class="img-fluid" src="img/post-5.jpg" alt="MVC">
        <h2>Planejamento</h2>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam nobis ratione eius itaque, id
            facilis
            eligendi libero corrupti, eaque cumque amet repellat? Optio eius voluptatibus possimus
            repellendus
            iusto
            nam sequi.</p>
    </div>
</div>
<div class="row">
<div class="col-12">

    <h3>FAÇA DIFERENTE...</h3>
    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugiat eius optio vitae sint libero
        obcaecati,
        quaerat commodi, aperiam voluptates tenetur sit laboriosam a dolor iure sequi ab! Labore, soluta
        laudantium.</p>


   
</div>
</div>
@endsection